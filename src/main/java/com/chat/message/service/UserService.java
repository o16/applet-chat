package com.chat.message.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chat.message.model.User;

public interface UserService extends IService<User> {
}
